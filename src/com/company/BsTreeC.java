package com.company;

import java.util.List;

public class BsTreeC implements BsTree{

    Node root;

    public BsTreeC() {
        root = null;
    }

    public BsTreeC(List<Person> persons) {
        persons.forEach(this::add);
    }

    @Override
    public void clear() {
        root = null;
    }

    @Override
    public int size() {
        return 0;
    }



    @Override
    public Person[] toArray() {
        return new Person[0];
    }

    @Override
    public Node add(Person val) {
        return append(root,val);
    }
    public Node append(Node node, Person value) {
        if (node == null) {
            root = new Node(value);
            return root;
        }
        if (value.compare(value, node.value) < 0) {
            if (node.left != null) {
                append(node.left, value);
            } else {

                node.left = new Node(value);
            }
        } else if (value.compare(value, node.value) > 0) {
            if (node.right != null) {
                append(node.right, value);
            } else {

                node.right = new Node(value);
            }
        }
        return node;
    }


    @Override
    public Person del(Person val) {
        return null;
    }

    @Override
    public int getWidth(Node node, int i) {
        return 0;
    }

    @Override
    public int getHeight() {
        return 0;
    }

    @Override
    public int nodes() {
        return 0;
    }

    @Override
    public int leaves() {
        return 0;
    }
}
