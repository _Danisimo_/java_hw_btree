package com.company;

public class BsTreeR implements BsTree  {

    private Node root;


    private Node addRecursive(Node current, Person value) {
        if (current == null){
            return new Node(value);
        }
        if(value.compare(value,root.value) < 0){

            current.left = addRecursive(current.left,value);

        }else if (value.compare(value,root.value) > 0){

            current.right = addRecursive(current.right,value);

        }
        return current;
    }

    @Override
    public void clear() {
        root = null;
    }

    @Override
    public int size() {
        return bTreeSize(root);
    }

    private int bTreeSize(Node root){
        if(root == null)
        {
            return 0;
        }
        else{
            return 1 + bTreeSize(root.left) + bTreeSize(root.right);
        }
    }

    @Override
    public Person[] toArray() {
        return new Person[0];
    }

    @Override
    public Node add(Person val) {
        root = addRecursive(root,val);

        return null;
    }

    @Override
    public Person del(Person val) {
        return null;
    }

    @Override
    public int getWidth(Node node, int i) {
        return getMaxWidth(root);
    }

    private int getMaxWidth(Node node)
    {
        int maxWidth = 0;
        int width;
        int h = height(node);
        int i;

        for (i = 1; i <= h; i++)
        {
            width = getWidth(node, i);
            if (width > maxWidth)
                maxWidth = width;
        }

        return maxWidth;
    }

    private int height(Node node)
    {
        if (node == null)
            return 0;
        else
        {
            int lHeight = height(node.left);
            int rHeight = height(node.right);
            return (lHeight > rHeight) ? (lHeight + 1) : (rHeight + 1);
        }
    }

    @Override
    public int getHeight() {
        return depth(root);
    }

    private int depth(Node node)
    {
        if (node == null)
            return 0;
        else
        {
            int lDepth = depth(node.left);
            int rDepth = depth(node.right);

            if (lDepth > rDepth) {
                return (lDepth + 1);
            }else{
                return (rDepth + 1);
            }
        }
    }

    @Override
    public int nodes() {
        return 0;
    }

    @Override
    public int leaves() {
        return getLeavsCount(root);
    }

    private int getLeavsCount(Node node){
        if( node == null){
            return 0;
        }
        if(node.left == null && node.right == null){
            return 1;
        }else{
            return getLeavsCount(node.left)+getLeavsCount(node.right);
        }
    }
}
