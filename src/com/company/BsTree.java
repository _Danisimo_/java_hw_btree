package com.company;

public interface BsTree {
    void clear();

    int size();

    Person[] toArray();

    String toString();

    Node add(Person val);

    Person del(Person val);

    int getWidth(Node node, int i);

    int getHeight();

    int nodes();

    int leaves();
}
