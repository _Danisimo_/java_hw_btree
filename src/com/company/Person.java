package com.company;

import java.util.Comparator;

public class Person implements Comparator<Person> {

    private String firstName;
    private String lastName;
    private int age;
    private String city;

    public Person(String fname, String lname, int age, String city) {
        this.firstName = fname;
        this.lastName = lname;
        this.age = age;
        this.city = city;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String fname) {
        this.firstName = fname;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName (String lname) {
        this.lastName = lname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", city='" + city + '\'' +
                '}';
    }

    @Override
    public int compare(Person person, Person t1) {
        return 0;
    }
}
